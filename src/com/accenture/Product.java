package com.accenture;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Product {
	@Id	
	private int pid;	
	private String pName;
	private int price;
	private int quantity;
	public Product() {
		super();
	}
	public Product(int pid, String pName, int price, int quantity) {
		super();
		this.pid = pid;
		this.pName = pName;
		this.price = price;
		this.quantity = quantity;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "Product [pid=" + pid + ", pName=" + pName + ", price=" + price + ", quantity=" + quantity + "]";
	}
	
}
